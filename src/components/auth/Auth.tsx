import LifeHook from '../../hooks/lifeHook';
import AuthCoreService from '../../services/core/auth';
import AuthState from '../../state/auth';
import APIService from '../../services/network/api'
import { Outlet, useNavigate } from 'react-router-dom';

export default function Auth() {
    // region TRIGGER
    LifeHook.useLife(onMounted);
    const navigate = useNavigate();
    // endregion

    // region METHODS
    async function onMounted() {
        await AuthCoreService.authorization(AuthState.token.value, APIService.authAPI, signInNavigator);
    }

    function signInNavigator() {
        navigate('/sign-in');
    }
    // endregion

    return (
        <Outlet />
    );
}
