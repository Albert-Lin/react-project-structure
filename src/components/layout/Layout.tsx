import { Outlet } from 'react-router-dom';

export default function Layout() {

    return (
        <div className="layout">
            <Nav />
            <div className="body-container">
                <SideMenu />
                <div className="main-container">
                    <Outlet />
                </div>
            </div>
        </div>
    );
}

function Nav() {

    return (
        <div className="nav"></div>
    );
}

function SideMenu() {

    return (
        <div className="side-menu"></div>
    );
}
