import { useEffect } from 'react';

function useLife(onMountedHandler: () => void, onDestroyedHandler?: () => void) {
    useEffect(() => {
        onMountedHandler();
        return onDestroyedHandler;
    }, []);
}

export default {
    useLife,
};
