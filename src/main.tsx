import React from 'react'
import ReactDOM from 'react-dom/client'
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import './assets/scss/styles/main.scss';
import AuthCoreService from './services/core/auth';
import AuthState from './state/auth';
import Auth from './components/auth/Auth.tsx';
import Layout from './components/layout/Layout.tsx';
import SignIn from './pages/SignIn.tsx';
import NotFound from './pages/NotFound.tsx';


AuthCoreService.loadTokenFromLocalStorage(AuthState.tokenLoader);


const router = createBrowserRouter([
    {
        path: '/*',
        element: <Auth />,
        children: [
            {
                path: '',
                element: <Layout />,
                children: [
                    {
                        path: '',
                        element: <>First Page</>,
                    }
                ]
            },
            {
                path: 'sign-in',
                element: <SignIn />,
            },
            {
                path: '*',
                element: <NotFound />,
            },
        ],
    },
]);


ReactDOM.createRoot(document.getElementById('root')!).render(
  <React.StrictMode>
      <RouterProvider router={router} />
  </React.StrictMode>,
)
