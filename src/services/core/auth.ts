import ConditionUtil from '../../utils/condition';

function loadTokenFromLocalStorage(loader: () => void) {
    loader();
}

async function authorization(token: string|null, authAPI: (token: string|null) => Promise<{ success: boolean; code: number; msg?: string; }|undefined>, signInNavigator: () => void) {
    let apiResult = false;
    const authAPIWrapper = async () => {
        const response = await authAPI(token);
        apiResult = !!response?.success && response?.code === 200;
        return apiResult;
    };

    await ConditionUtil.switchCase([
        {
            condition: () => !token,
            exec: () => signInNavigator(),
        },
        {
            condition: async () => await authAPIWrapper(),
            exec: () => apiResult ? ConditionUtil.skip() : signInNavigator(),
        },
        {
            condition: () => true,
            exec: () => signInNavigator(),
        }
    ]);
}


export default {
    loadTokenFromLocalStorage,
    authorization,
};
