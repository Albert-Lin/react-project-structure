import { Signal, signal } from '@preact/signals';

type CreateSignalStateOptions = {
    localstorageKey?: string;
};

function createSignalState<T>(value: T, opts?: CreateSignalStateOptions): [ data: Signal<T>, mutator: (value: T) => void, loader: () => void  ] {
    const data = signal<T>(value);
    const mutator = (value: T) => {
        if (data.value === value) return;
        opts?.localstorageKey && window.localStorage.setItem(opts.localstorageKey, JSON.stringify(value));
        data.value = value;
    };
    const loader = () => {
        if (!opts?.localstorageKey) return;
        data.value = JSON.parse(window.localStorage.getItem(opts.localstorageKey) as string);
    };

    return [ data, mutator, loader ];
}

export default {
    createSignalState,
};
