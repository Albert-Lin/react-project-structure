async function authAPI(token: string|null) {
    // TODO: implement with invoke actual API
    return {
        success: true,
        code: 200,
        msg: `Invalid Token: ${ token }`,
    };
}

export default {
    authAPI,
};
