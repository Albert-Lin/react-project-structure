import SignalExpansionService from '../services/expansion/signal';

const [ token, setToken, tokenLoader ] = SignalExpansionService.createSignalState<string|null>(null, { localstorageKey: 'token' });




export default {
    token,
    setToken,
    tokenLoader,
};
