export type SyncSwitchCase = {
    condition: () => boolean;
    exec: () => unknown;
};

export type SwitchCase = {
    condition: () => boolean|Promise<boolean>;
    exec: () => unknown|Promise<unknown>;
};

export type SWitchCaseOptions = {
    mode?: 'find'|'filter'; // default as 'find'
    ignoreException?: boolean; // default as true
};

function switchCaseSync(cases: Array<SyncSwitchCase>, options?: SWitchCaseOptions) {
    const mode = options?.mode || 'find';
    const ignoreException = options?.ignoreException || true;
    const caseMeetCondition = ({ condition }: SyncSwitchCase) => {
        try {
            return condition();
        } catch (e) {
            if (ignoreException) return false;
            throw e;
        }
    };
    const toExecutorResult = ({ exec }: SyncSwitchCase) => {
        try {
            return exec();
        } catch (e) {
            if (ignoreException) return;
            throw e;
        }
    };

    if (mode === 'find') {
        const target = cases.find(caseMeetCondition);
        return target?.exec();
    }

    if (mode === 'filter') {
        const target = cases.filter(caseMeetCondition);
        return target.map(toExecutorResult);
    }

    return;
}


async function switchCase(cases: Array<SwitchCase>, options?: SWitchCaseOptions) {
    const mode = options?.mode || 'find';
    const ignoreException = options?.ignoreException || true;
    const caseMeetCondition = async ({ condition }: SwitchCase) => {
        try {
            return await condition();
        } catch (e) {
            if (ignoreException) return false;
            throw e;
        }
    };
    const toExecutorResult = async ({ exec }: SwitchCase) => {
        try {
            return await exec();
        } catch (e) {
            if (ignoreException) return;
            throw e;
        }
    };

    if (mode === 'find') {
        for (const caseItem of cases) {
            if (await caseMeetCondition(caseItem)) {
                return await toExecutorResult(caseItem);
            }
        }
        return;
    }

    if (mode === 'filter') {
        const verifyResults = await Promise.all(cases.map(caseMeetCondition));
        const validCases = verifyResults
            .map((verifyResult, index) => [ verifyResult, index ] as [ boolean, number ])
            .filter(([ verifyResult ]) => verifyResult)
            .map(([ , index]) => cases[index]);
        return await Promise.all(validCases.map(toExecutorResult));
    }

    return;
}

function skip() {}

export default {
    switchCaseSync,
    switchCase,
    skip,
};
