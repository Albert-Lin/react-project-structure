import AuthCoreService from '../../../src/services/core/auth';

describe('Auth Core Service', () => {

    describe('loadTokenFromLocalStorage', () => {
        it('Correct - Success', () => {
            // arrange
            const expectation = true;
            let actual = false;
            const mockLoader = () => {
                actual = true;
            };

            // act
            AuthCoreService.loadTokenFromLocalStorage(mockLoader);

            // assert
            expect(actual).toBe(expectation);
        });
    });

    describe('authorization', () => {
        it('Correct - Fail on no token - null', async () => {
            // arrange
            const expectation = true;
            let navigated = false;
            const token = null;
            const mockSignInNavigator = () => navigated = true;
            const mockAuthAPI = async () => ({ success: false, code: 9999, msg: 'Testing Error' });

            // act
            await AuthCoreService.authorization(token, mockAuthAPI, mockSignInNavigator);

            // assert
            expect(navigated).toBe(expectation);
        });

        it('Correct - Fail on no token - empty string', async () => {
            // arrange
            const expectation = true;
            let navigated = false;
            const token = '';
            const mockSignInNavigator = () => navigated = true;
            const mockAuthAPI = async () => ({ success: false, code: 9999, msg: 'Testing Error' });

            // act
            await AuthCoreService.authorization(token, mockAuthAPI, mockSignInNavigator);

            // assert
            expect(navigated).toBe(expectation);
        });

        it('Correct - Success on correct token authorize by Auth API', async () => {
            // arrange
            const expectation = false;
            let navigated = false;
            const token = 'token';
            const mockSignInNavigator = () => navigated = true;
            const mockAuthAPI = async () => ({ success: true, code: 200 });

            // act
            await AuthCoreService.authorization(token, mockAuthAPI, mockSignInNavigator);

            // assert
            expect(navigated).toBe(expectation);
        });

        it('Correct - Fail on incorrect token authorize by Auth API', async () => {
            // arrange
            const expectation = true;
            let navigated = false;
            const token = 'token';
            const mockSignInNavigator = () => navigated = true;
            const mockAuthAPI = async () => ({ success: false, code: 401, msg: 'Invalid Token' });

            // act
            await AuthCoreService.authorization(token, mockAuthAPI, mockSignInNavigator);

            // assert
            expect(navigated).toBe(expectation);
        });

        it('Correct - Fail on API exception', async () => {
            // arrange
            const expectation = true;
            let navigated = false;
            const token = 'token';
            const mockSignInNavigator = () => navigated = true;
            const mockAuthAPI = async () => {
                throw new Error('API Error');
            };

            // act
            await AuthCoreService.authorization(token, mockAuthAPI, mockSignInNavigator);

            // assert
            expect(navigated).toBe(expectation);
        });
    });
});
